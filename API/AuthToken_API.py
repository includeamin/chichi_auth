from flask import Blueprint, request
from Classes.AuthClasses import AuthToken
from Classes.Tools import Tools
from Middleware.middleware import validate_request
Result = Tools.Result
Error = Tools.errors

AuthToken_Route = Blueprint("AuthToken", __name__, "template")


@AuthToken_Route.route('/system/users/token/add', methods=["POST"])
@validate_request
def add_token():
    if request.form is None:
        return Result(False, Error("UIN"))
    return AuthToken.__add__(request.form["UserId"])


@AuthToken_Route.route('/users/auth/check/<user_id>/<token>')
@validate_request
def check_user_auth(user_id, token):
    return AuthToken.is_auth(user_id, token)


@AuthToken_Route.route('/system/users/logout/<user_id>/<token>')
@validate_request
def log_out(user_id,token):
    return AuthToken.log_out(user_id,token)
