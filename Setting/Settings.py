from pydantic import BaseSettings


class DataBaseSetting(BaseSettings):
    DATABASE_URL: str = 'localhost:27017'
    PERMISSION_COLLECTION_NAME: str = "Permissions"
    ROLE_PERMISSION_COLLECTION_NAME: str = "Permission"
    DATABASE_NAME: str = "RoleService"
    ROLE_COLLECTION: str = "Roles"
    USER_ROLE: str = "UserRole"
    AUTH_TOKEN: str = "AuthToken"


class Bridges(BaseSettings):
    pass


database_settings = DataBaseSetting()
