import requests
import json
import pymongo
import os
from  Classes.Tools import Tools
Decode = Tools.decode
Initer = Tools.initer



dirpath = os.getcwd()

with open(os.path.join(dirpath,"Database/databaseConfig.json")) as f:
    configs = json.load(f)

get_database_url = json.loads(
    requests.get("{0}Services/config/get/AuthenticationService".format(configs['MicroServiceManagementURl']),
               ).content)




if not get_database_url["State"]:
    exit(1)

get_database_url = json.loads(get_database_url["Description"])

url= ''
if get_database_url["Key"] == None:
    url = Decode(get_database_url["DatabaseString"])
else:
    Initer(get_database_url["Key"])
    url = Decode(get_database_url["DatabaseString"])


print(url)



mongodb = pymongo.MongoClient(url)
database = mongodb[configs["RoleServiceDatabaseName"]]
roledb = database[configs["RoleCollection"]]
userroledb = database[configs["UserRole"]]
authtokendb = database[configs["Auth_Token"]]
permissiondb = database[configs["PermissionDbName"]]
rolepermissiondb = database[configs["RolesPermissionsDbName"]]


