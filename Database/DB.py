import requests
import json
import pymongo
import os
from Classes.Tools import Tools
from Setting.Settings import database_settings

# Decode = Tools.decode
# Initer = Tools.initer
#
# dirpath = os.getcwd()
#
# with open(os.path.join(dirpath, "Database/databaseConfig.json")) as f:
#     configs = json.load(f)
#
# get_database_url = json.loads(
#     requests.get("{0}Services/config/get/AuthenticationService".format(configs['MicroServiceManagementURl']),
#                  ).content)
#
# if not get_database_url["State"]:
#     exit(1)
#
# get_database_url = json.loads(get_database_url["Description"])
#
# url = ''
# if get_database_url["Key"] == None:
#     url = Decode(get_database_url["DatabaseString"])
# else:
#     Initer(get_database_url["Key"])
#     url = Decode(get_database_url["DatabaseString"])
#
# print(url)

mongodb = pymongo.MongoClient(database_settings.DATABASE_URL)
database = mongodb[database_settings.DATABASE_NAME]
roledb = database[database_settings.ROLE_COLLECTION]
userroledb = database[database_settings.USER_ROLE]
authtokendb = database[database_settings.AUTH_TOKEN]
permissiondb = database[database_settings.PERMISSION_COLLECTION_NAME]
rolepermissiondb = database[database_settings.ROLE_PERMISSION_COLLECTION_NAME]

# jwt_tokens_collection = database["jwt_tokens"]
# jwt_permission_collection = database['jwt_permissions']
# jwt_roles_collection = database['jwt_roles']
# jwt_user_permissions_collection = database['jwt_user_permissions']
# jwt_logs_collection = database["jwt_logs"]
