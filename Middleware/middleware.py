from flask import request
from functools import wraps
def validate_request(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # validate access
        return f(*args, **kwargs)
    return decorated_function
