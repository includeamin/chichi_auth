import datetime
from Database.DB import permissiondb, userroledb, rolepermissiondb, roledb, authtokendb
from Classes.Tools import Tools
from secrets import token_hex

Result = Tools.Result
Error = Tools.errors
combinetwolist = Tools.combinetwolist
IsValidId = Tools.IsValidId


class Permissions:
    def __init__(self, name, description):
        self.Name = name
        self.Created_at = datetime.datetime.now()
        self.Updated_at = datetime.datetime.now()
        self.Description = description

    @staticmethod
    def __add__(permission_name, description):
        try:
            exist = permissiondb.find_one({"Name": permission_name})
            if exist is None:
                permissiondb.insert_one(Permissions((str(permission_name).upper()), description).__dict__)
                return Result(True, "D")

            return Result(False, Error("IAE"))
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def CheckPermission(user_id, permission):
        try:
            permission_exist = permissiondb.find_one({"Name": permission})
            if permission_exist is None:
                return Result(False, "this permission not exist")
            permission_id = permission_exist["_id"]

            user_has_role = userroledb.find_one({"User_Id": user_id})
            if user_has_role is None:
                return Result(False, Error('UDHR'))
            users_rols_permissions = rolepermissiondb.find_one({"RolesId": user_has_role["RoleId"]})
            if users_rols_permissions is None:
                return Result(False, Error("RNF"))
            permissionList = list(users_rols_permissions["RolesPermissionsIds"])
            if permissionList.__contains__(permission_id):
                return Result(True, Error("PA"))
        except Exception as ex:
            return Result(ex.args)


class User_Role:
    def __init__(self, user_id, role_id):
        self.User_Id = user_id
        self.RoleId = role_id
        self.Created_at = datetime.datetime.now()
        self.Updated_at = datetime.datetime.now()

    @staticmethod
    def __add__(user_id, role_name):
        try:
            if not IsValidId(user_id):
                return Result(False, Error("IVID"))
            role = roledb.find_one({"Name": role_name})
            if role is not None:
                roleid = role["_id"]
                exist = userroledb.find_one({"User_Id": user_id})
                if exist is None:
                    userroledb.insert_one(User_Role(user_id, roleid).__dict__)
                    return Result(True, "D")
                else:
                    userroledb.update_one({"User_Id": user_id},
                                          {"$set": {"RoleId": roleid, "Updated_at": datetime.datetime.now()}})
                    return Result(True, "D")
            else:
                return Result(False, Error("RNF"))
        except Exception as ex:
            return Result(False, ex.args)


class Role_Permission:
    def __init__(self, rolesId, permissions_Id_list):
        self.RolesId = rolesId
        self.RolesPermissionsIds = permissions_Id_list
        self.Created_at = datetime.datetime.now()
        self.Updated_at = datetime.datetime.now()

    @staticmethod
    def __add__(role_name, list_of_permissions):
        try:
            role = roledb.find_one({"Name": role_name})
            permission_id_list = str(list_of_permissions).split(',')

            if role is not None:
                roleId = role["_id"]
                permissionIdList = []
                for per in permission_id_list:
                    temp_permission = permissiondb.find_one({"Name": per})
                    if temp_permission is None:
                        return Result(False,
                                      "one or some of permissions is not exist or invalid , check and re-request again")
                    else:
                        permissionIdList.append(temp_permission["_id"])
                temp_roles_permission = Role_Permission(roleId, permissionIdList)
                exist = rolepermissiondb.find_one({"RolesId": roleId})
                if exist is not None:
                    oldperlist = list(exist["RolesPermissionsIds"])
                    newlist = combinetwolist(oldperlist, permissionIdList)
                    rolepermissiondb.update_one({"RolesId": roleId}, {
                        "$set": {"RolesPermissionsIds": newlist, "Updated_at": datetime.datetime.now()}})
                    return Result(False, "Same roles permission already defined , but permissions are updated")
                else:
                    rolepermissiondb.insert_one(temp_roles_permission.__dict__)
                    return Result(True, "D")
            else:
                return Result(False, Error("INF"))
        except Exception as ex:
            return Result(False, ex.args)


class Role:
    def __init__(self, name, description):
        self.Name = name
        self.Description = description
        self.Created_at = datetime.datetime.now()
        self.Updated_at = datetime.datetime.now()

    @staticmethod
    def __add__(name, description):
        try:
            exist = roledb.find_one({"Name": name})
            if exist is None:
                roledb.insert_one(Role(name, description).__dict__)
                return Result(True, "D")
            return Result(False, Error("IAE"))
        except Exception as ex:
            return Result(False, ex.args)
    @staticmethod
    def __delete__(self, instance):
        pass


class AuthToken:
    def __init__(self, userid, token):
        self.UserId = userid
        self.Token = token
        self.Created_at = datetime.datetime.now()
        self.Updated_at = datetime.datetime.now()

    @staticmethod
    def __add__(user_id):
        try:
            token_db = authtokendb.find_one({"UserId": user_id})
            token = AuthToken.token_generator()

            if token_db:

                authtokendb.update_one({"_id": token_db["_id"]},
                                       {"$set": {"Token": token, "Updated_at": datetime.datetime.now()}})
                return Result(True,token)
            else:

                authtokendb.insert_one(AuthToken(user_id, token).__dict__)

            return Result(True, token)

        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def log_out(user_id,token):
        try:
            authtokendb.find_one_and_delete({"UserId": user_id,"Token":token})
            return Result(True, "D")
        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def is_auth(user_id, token):
        try:
            token_db = authtokendb.find_one({"Token": token,"UserId":user_id})
            if token_db:
                return Result(True,"D")
            else:
                return Result(False,"N")

        except Exception as ex:
            return Result(False, ex.args)

    @staticmethod
    def token_generator():
        try:
            return token_hex()
        except Exception as ex:
            return Result(False, ex.args)


class AuthTokenMail:
    def __init__(self, userid, token, email):
        self.UserId = userid
        self.Email = email
        self.Token = token
        self.Created_at = datetime.datetime.now()
        self.Updated_at = datetime.datetime.now()

    @staticmethod
    def __add__(self, other):
        pass


class Actions:
    pass
